var helpers = require('../helpers'),
    fs = require('fs'),
    path = require('path'),
    item_manager = require('../item-manager'),
    web_logic = require('./logic'),
    configuration = require('./configuration'),
    stylus = require('stylus'),
    nib = require('nib'),
    express = require('express');

var atlasboard = {};

module.exports = function(app, port, packagesPath, generalConfigManager) {

  //generalConfigManager.atlasboardAssetFolder and generalConfigManager.wallboardAssetFolder are injected in testing env.
  var atlasboard_assets_folder = generalConfigManager.atlasboardAssetFolder || path.join(__dirname, "../../assets");
  var wallboard_assets_folder = generalConfigManager.wallboardAssetFolder || path.join(process.cwd(), "assets");

  // -----------------------------------------
  // Web server configuration
  // -----------------------------------------
  app.configure(function() {
    app.set('port', port);
    app.use(express.logger('dev'));
    app.use(express.compress());
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(app.router);
    app.use(express.errorHandler());
  });

  app.configure('development', function(){
    app.use(express.errorHandler());
  });

  var compiledAssetsFolder = path.join(wallboard_assets_folder, 'compiled');
  app.use(stylus.middleware({
    src: atlasboard_assets_folder,
    dest: compiledAssetsFolder,
    compile: function(str, path) { // optional, but recommended    
    return stylus(str)
      .set('filename', path)
      .set('warn', false)
      .set('compress', true)
      .use(nib());
    }
  }));

  // -----------------------------------------
  //  Expose both, wallboard assets and atlasboard assets.
  //  Wallboard assets take precedence
  // -----------------------------------------
  app.use(express.static(wallboard_assets_folder));
  app.use(express.static(compiledAssetsFolder));
  app.use(express.static(atlasboard_assets_folder));

  // -----------------------------------------
  //  Log
  // -----------------------------------------
  app.get("/log", function(req, res) {
    if (generalConfigManager.get("live-logging").enabled === true){
      web_logic.log(req, res);
    }
    else{
      res.end('live logging it disabled. Must be enabled in config file');
    }
  });


  // -----------------------------------------
  //  Basic authentication of the dashboards
  // -----------------------------------------
  var auth = function(req,res,next) {
    return express.basicAuth(function(user, pass, callback) {
          web_logic.getDashboardPassword(packagesPath, req.params.dashboard, function(err,password) {
            if(err) return next(err);
               var result = (user === req.params.dashboard && pass === password);
               callback(null, result);
          });
          },"User Name: " + req.params.dashboard)(req,res,next);
  }



  // -----------------------------------------
  //  Create a new dashboard
  // -----------------------------------------
  app.post("/", function(req, res) {
    configuration.createDashboard(packagesPath, req, res);
  });

  // -----------------------------------------
  //  Save the dashboard layout
  // -----------------------------------------
  app.post("/:dashboard/layout",auth, function(req, res) {
    web_logic.saveLayout(packagesPath,req.params.dashboard, req, res);
  });


  // -----------------------------------------
  //  List possible widgets
  // -----------------------------------------
  app.get("/:dashboard/widgets",auth, function(req, res) {
    configuration.listWidgets(packagesPath,req.params.dashboard, req, res);
  });

  // -----------------------------------------
  //  Add a widget
  // -----------------------------------------
  app.post("/:dashboard/widgets/add",auth, function(req, res) {
    configuration.addWidget(packagesPath,req.params.dashboard, req, res);
  });


  // -----------------------------------------
  //  Remove a widget
  // -----------------------------------------
  app.delete("/:dashboard/widgets/:widgetId",auth, function(req, res) {
    configuration.deleteWidget(packagesPath,req.params.dashboard,req.params.widgetId, req, res);
  });


  // -----------------------------------------
  //  Fetch configuration for specific widget
  // -----------------------------------------
  app.get("/:dashboard/widgets/:widget/config",auth, function(req, res) {
    configuration.getWidgetConfig(packagesPath,req.params.dashboard, req.params.widget, req, res);
  });

  // -----------------------------------------
  //  Save the configuration for specific widget
  // -----------------------------------------
  app.post("/:dashboard/widgets/:widget/config",auth, function(req, res) {
    configuration.saveWidgetConfig(packagesPath,req.params.dashboard, req.params.widget, req, res);
  });



  // -----------------------------------------
  //  Fetch resources for specific widget
  // -----------------------------------------
  app.get("/widgets/resources", function(req, res) {
    web_logic.renderWidgetResource(path.join(process.cwd(), 'packages'), req.query.resource, req, res);
  });

  // -----------------------------------------
  //  Fetch JS for specific widget
  // -----------------------------------------
  app.get("/widgets/:widget/js", function(req, res) {
    web_logic.renderJsWidget(packagesPath, req.params.widget, req, res);
  });

  // -----------------------------------------
  //  Fetch HTML and CSS for specific widget
  // -----------------------------------------
  app.get("/widgets/:widget", function(req, res) {
    web_logic.renderHtmlWidget(packagesPath, req.params.widget, req, res);
  });



  // -----------------------------------------
  //  Entry point for a particular dashboard
  // -----------------------------------------
  app.get("/:dashboard",auth, function(req, res) {
    web_logic.renderDashboard(packagesPath, req.params.dashboard, req, res);
  });

  // -----------------------------------------
  //  Entry point for a particular dashboard
  // -----------------------------------------
  app.get("/:dashboard/js",auth, function(req, res) {
    web_logic.renderJsDashboard(packagesPath, wallboard_assets_folder, req.params.dashboard, req, res);
  });




  // -----------------------------------------
  // No address given - list all available dashboards
  // -----------------------------------------
  app.get("/", function(req, res) {
    web_logic.listAllDashboards(packagesPath, req, res);
  });

};