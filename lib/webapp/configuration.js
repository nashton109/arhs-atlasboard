var helpers = require('../helpers'),
    fs = require('fs'),
    path = require('path'),
    itemManager = require('../item-manager'),
    logger = require('../logger')(),
    jobs_manager = require('../job-manager'),
    _ = require ('underscore'),
    generalConfigManager = require('../config-manager')(),
    scheduler = require('../scheduler').scheduler;

var exclude = ["widget","job","fakeusernameremembered","fakepasswordremembered"];

function getSafeItemName (item_name){
  return path.basename(item_name).split('.')[0];
}

function getDashboardsFolder(packagesPath) {
  return packagesPath[0] + '\\default\\dashboards\\'
}

function getDashboardConfiguration(packagesPath,dashboardName) {
  return getDashboardsFolder(packagesPath) + dashboardName +".json"; 
}


module.exports = {

    // ---------------------------------------------------------------
    // Create a new dashboard with the default configuration and the entered password.
    // ---------------------------------------------------------------
    createDashboard : function (packagesPath, req, res){
      var dashboardFolder = getDashboardsFolder(packagesPath);
      var name = req.body.name;
      var password = req.body.password;

      var newDashboard =  dashboardFolder + name + ".json";

      if (fs.existsSync(newDashboard)){
          return res.status(500).send('Dashboard "'+ name +'" already exists.');
      }

      var dashboardConfiguration =   _.clone(emptyDashboard);
      dashboardConfiguration.password = password;

      fs.writeFile(newDashboard, JSON.stringify(dashboardConfiguration, null, 4),function(err) {
            if(err) return res.status(500).send(err);
            res.redirect("/"+name);
       });


    },


    // ---------------------------------------------------------------
    // Add a new widget. Schedule the related job.
    // ---------------------------------------------------------------
    addWidget : function (packagesPath, dashboardName, req, res){


         var widgetDefault = generalConfigManager.get('widgets')[req.body.widget];

         readDashboardConfiguration(packagesPath,dashboardName,function(err,dashboard,dashboardConfiguration) {



          if(err) return res.status(500).send(err);
          
          var widgetNumber = getWidgetNumber(dashboardConfiguration,req.body.widget);
          var configId = req.body.widget + widgetNumber;

          var widget = {
                row: 1,
                col: 1,
                width: widgetDefault.width ? widgetDefault.width : 1,
                height:  widgetDefault.height ? widgetDefault.height : 1,
                widget: req.body.widget,
                job: req.body.job,
                config: configId
            };

          dashboardConfiguration.layout.widgets.push(widget);

          dashboardConfiguration.config[configId] = configFromBody(req.body);


          fs.writeFileSync(dashboard, JSON.stringify(dashboardConfiguration, null, 4),"utf8");

          jobs_manager.get_job(packagesPath,dashboardName,widget.job,configId, function(err,job) {
              if(err) return res.status(500).send(err);
              scheduler.scheduleFirst(job,false);

              res.redirect('/' + dashboardName);
          });
        

        });
      },

          // ---------------------------------------------------------------
    // Add a new widget. Schedule the related job.
    // ---------------------------------------------------------------
    deleteWidget : function (packagesPath, dashboardName, eventId,req, res){
      var widget = eventId.split("_")[2];
      var configId = eventId.split("_")[1];


      readDashboardConfiguration(packagesPath,dashboardName,function(err,dashboard,dashboardConfiguration) {

          if(err) return res.status(500).send(err);


          dashboardConfiguration.layout.widgets = _.filter(dashboardConfiguration.layout.widgets, function(widget){ return widget.config !== configId });
          delete dashboardConfiguration.config[configId];

          fs.writeFileSync(dashboard, JSON.stringify(dashboardConfiguration, null, 4),"utf8");

          // Unschedule job.
          scheduler.removeJob(eventId);

          res.send(200);
        });
      },

      listWidgets: function (packagesPath, dashboardName,req, res){
        readDashboardConfiguration(packagesPath,dashboardName,function(err,dashboard,dashboardConfiguration) {
          
          var widgets = _.clone(generalConfigManager.get('widgets'));

         /* dashboardConfiguration.layout.widgets.forEach(function(widget) {
              delete widgets[widget.widget];
          });*/

          res.json(widgets);

        });

      },

      // ---------------------------------------------------------------
      // Return the JSON configuration for a widget
      // ---------------------------------------------------------------
      getWidgetConfig: function (localPackagesPath,dashboardName, eventId, req, res){


        var widget = eventId.split("_")[2];
        var configId = eventId.split("_")[1];

        var widgetDefaultConfig = generalConfigManager.get('widgets')[widget];

        itemManager.get_first(localPackagesPath, dashboardName, "dashboards", ".json", function(err, dashboard){
          if (err || !dashboard){
            return res.send(err ? 400 : 404, err ? err : "Trying to render dashboard " + dashboardName + ", but couldn't find any dashboard in the packages folder");
          }
          var jsonBody;
          try {
            jsonBody = JSON.parse(fs.readFileSync(dashboard));
          }
          catch(e){
            return res.send(400, "Invalid dashboard config file");
          }

          var config = jsonBody.config[configId];
          

          for(prop in config) {
            var defaultProp = widgetDefaultConfig.config[prop];
            if(defaultProp.type != 'password') {
              defaultProp.value = config[prop];
            } else {
              defaultProp.value = Array(config[prop].length+1).join("*");
            }            
          }

          res.json(widgetDefaultConfig);
        });
      },


      // ---------------------------------------------------------------
      // Return the JSON configuration for a widget
      // ---------------------------------------------------------------
      saveWidgetConfig: function (localPackagesPath,dashboardName, eventId, req, res){
        var widget = eventId.split("_")[2];
        var configId = eventId.split("_")[1];

        var widgetDefaultConfig = generalConfigManager.get('widgets')[widget];

        itemManager.get_first(localPackagesPath, dashboardName, "dashboards", ".json", function(err, dashboard){
          if (err || !dashboard){
            return res.send(err ? 400 : 404, err ? err : "Trying to render dashboard " + dashboardName + ", but couldn't find any dashboard in the packages folder");
          }
          var jsonBody;
          try {
            jsonBody = JSON.parse(fs.readFileSync(dashboard));
          }
          catch(e){
            return res.send(400, "Invalid dashboard config file");
          }


          var config = configFromBody(req.body);

          for(prop in widgetDefaultConfig.config) {
            if(widgetDefaultConfig.config[prop].type != 'password') {
              jsonBody.config[configId][prop] = config[prop];
            } else {
              // Only update password if not all stars
              if(!config[prop].match(/^\*+$/))
              {
                jsonBody.config[configId][prop] = config[prop];
              }
            }            
          }


          fs.writeFileSync(dashboard, JSON.stringify(jsonBody, null, 4));

          scheduler.updateConfig(eventId,jsonBody.config[configId],req.body);

          res.redirect('/' + dashboardName);
        });
      },
};


function readDashboardConfiguration(localPackagesPath, dashboardName, cb) {
  var dashboard = getDashboardConfiguration(localPackagesPath,dashboardName);

      if (!dashboard){
        return cb("The configuration of the dashboard " + dashboardName + " couldn't be found.");
      }
      var jsonBody;
      try {
       return  cb(null,dashboard,JSON.parse(fs.readFileSync(dashboard,"utf8")));
      }    catch(e){
        return cb(e);
      }
}

function configFromBody(body) {
  var config = {};

  for(prop in body) {
    if(exclude.indexOf(prop) == -1){ 

      var value = body[prop];
      if(isNumber(value)) {
        config[prop] = parseInt(value);
      } else {
        config[prop] = value;
      }
    }
  }

  return config;
}

function isNumber(str) {
   var pattern = /^\d+$/;
   return pattern.test(str);  // returns a boolean
}


/**
* If a widget is added twice the configuration should be named differently.
*/
function getWidgetNumber(dashboardConfiguration,widgetName) {
  var n = 0;

  dashboardConfiguration.layout.widgets.forEach(function(widget) {
    if(widget.widget == widgetName) {
      var m = parseInt(widget.config.substring(widgetName.length));
      if(m > n) {
        n = m;
      }
    }
  });

  return n+1;
}


var emptyDashboard= {
    "layout": {
        "title": false,
        "customJS": [
            "jquery.peity.js",
            "pretty.js",
            "highcharts.js",
            "themes/dark-unica.js"
        ],
        "widgets": []
    },
    "config": {}
};