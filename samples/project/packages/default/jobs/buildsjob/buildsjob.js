/*
	Builds overview Job
 */

var request = require('request');
var URLParser = require('../../service/urlparser').URLParser;
 

module.exports = function(config, dependencies, job_callback) {

	var buildsInfo = [];
	var success = true;
	var errorMessage = "";
	var errorJobs =  [];
	
	if(config.jobs && config.jobs.length > 0){
		config.jobs.forEach(function(job){	
			var urlLastBuildObject = new URLParser(config.jenkinsUrl,"/job/"+job.name+"/lastBuild/api/json");
			var optionsLastBuild = {
				url : urlLastBuildObject.getNormalizedUrl(),
				auth: {
						'user': config.username,
						'pass': config.apiKey				
					}
			};
			request(optionsLastBuild, function (error, response, body) {
				  
				if (error){
					var url = "<a href='"+optionsLastBuild.url+"'>"+optionsLastBuild.url+"</a>";
					return job_callback("Could not find Jenkins instance at "+ url); 
				}			
				if (!error && response.statusCode == 200) {
					  var data = JSON.parse(body);				  
					  var fullDisplayName = data.fullDisplayName;
					  var displayName = fullDisplayName.substring(0, fullDisplayName.indexOf(" #"));
					  
					  buildsInfo.push({   
									  job:job.name, 
									  displayName : displayName, 
									  result : data.result, 
									  isBuilding : data.building, 
									  buildEstimationTime : data.estimatedDuration,
									  buildStartTime : data.timestamp,
									  testResults : getTestResults(data)
									});
										 
				} else {
					errorJobs.push(job.name);
					success = false;
					if(response.statusCode == 403 || response.statusCode == 401 ){
						errorMessage += "Error " + response.statusCode + ": Cannot access build info for job "+job.name+". Check your credentials. <br>";
					} else{
						errorMessage += "Error " + response.statusCode + ": Job "+ job.name+ " has not been found. <br> ";
					}	
				}		
				 // synchronize jobs
				  if(buildsInfo.length + errorJobs.length === config.jobs.length){
					if(success){
						return job_callback(null,buildsInfo);
					} else{			
						return job_callback(errorMessage);					  
					}
				}		     
			});				
			
		})
	} else {
	
		return job_callback("No jobs defined in the parameters");
	}
};
				
	

function getTestResults(jsonData){

	var testResults = {}
	for(var i = 0; i < jsonData.actions.length; i++){
	
		if(jsonData.actions[i].failCount != undefined && jsonData.actions[i].skipCount != undefined && jsonData.actions[i].totalCount != undefined ){
			
			testResults.failCount = jsonData.actions[i].failCount;
			testResults.skipCount = jsonData.actions[i].skipCount;
			testResults.totalCount = jsonData.actions[i].totalCount - jsonData.actions[i].skipCount ;
			testResults.passedCount = jsonData.actions[i].totalCount - jsonData.actions[i].failCount - jsonData.actions[i].skipCount ;		
			return testResults;
		}
	}
	
	return null;
		
}
 








