/*
	Integration tests Job
 */

var request = require('request');
var URLParser = require('../../service/urlparser').URLParser;

module.exports = function(config, dependencies, job_callback) {

	// last build test results
	var urlLastBuildObject = new URLParser(config.jenkinsUrl,"/job/"+config.smokeJob+"/lastBuild/api/json");
	var optionsLastBuild = {
		url : urlLastBuildObject.getNormalizedUrl(),
		auth: {
				'user': config.username,
				'pass': config.apiKey				
			}
	};
	
	var urlTestsObject = new URLParser(config.jenkinsUrl,"/job/"+config.smokeJob+"/lastBuild/testReport/api/json");
	var optionsTests = {
		url : urlTestsObject.getNormalizedUrl(),
		auth: {
				'user': config.username,
				'pass': config.apiKey				
			}
	};

	
	request(optionsLastBuild, function (error, response, body) {
		
		var data = [];
		data.push(config.integrationJob);
		handleRequestResponse(error,response,function(){		
			var buildInfo = JSON.parse(body);
			if(buildInfo.result == 'FAILURE'){
				job_callback(null,data);
			} else{
				request(optionsTests, function (err, res, body) {
					data.push(config.limitFailed);	
					handleRequestResponse(err,res,function(){
						data.push(JSON.parse(body));
						job_callback(null,data);
					});
							
				});
			}		
		});
		
	});
	
	function handleRequestResponse(error,response,cb){
		var url = "<a href='"+optionsLastBuild.url+"'>"+optionsLastBuild.url+"</a>";
		if (error){ return job_callback("Could not find Jenkins instance at "+ url); }
		if (!error && response.statusCode == 200) {
			cb();			
		} else{
			if(response.statusCode == 404 || response.statusCode == 500 ){
					job_callback("Error " + response.statusCode + ": Job "+ config.smokeJob+ " has not been found. ");
			} else if(response.statusCode == 403 || response.statusCode == 401 ){
					job_callback("Error " + response.statusCode + ": Cannot access the build info for job "+ config.smokeJob+". Check your credentials");
			} else{
					job_callback("Error Jenkins " + response.statusCode);
			}	
		}
			
	}

	

};