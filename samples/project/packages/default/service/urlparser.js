var urlModule = require('url');

/**
	Module for normalizing urls.
*/


function UrlParser(host, path) {
	this.host = host;
	this.path = path;
}

UrlParser.prototype.getNormalizedUrl = function (defaultProtocol,cb) {
	var that = this;
	var normalizedUrl;
	var parsedUrl = urlModule.parse(that.host,false,true);
	
	if(!parsedUrl.protocol || !isNaN(parsedUrl.hostname)){	
		normalizedUrl = defaultProtocol ? defaultProtocol+'://'+that.host : 'http://'+that.host;
	} else{
		normalizedUrl = that.host;
	}

	if(normalizedUrl.charAt(normalizedUrl.length-1) == "/") {		
        normalizedUrl =  normalizedUrl.substr(0, normalizedUrl.length - 1);
    }
	
	if(that.path){
		normalizedUrl += that.path;
	}
	
	return normalizedUrl;				
};


UrlParser.prototype.getHostWithoutProtocol = function (cb) {
	var that = this;
	var formattedHost = that.host;
	if(formattedHost.charAt(formattedHost.length-1) == "/") {
		
        formattedHost = formattedHost.substr(0, formattedHost.length - 1);
    }
	return formattedHost.replace(/.*?:\/\//g, "");		
};



module.exports.URLParser = UrlParser;