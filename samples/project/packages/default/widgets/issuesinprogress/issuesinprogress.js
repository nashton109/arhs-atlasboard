widget = {
    //runs when we receive data from the job



    onData: function(el, data) {
        
		function getAvatar (issue){
		  if(issue.avatarUrl) {
				return $("<img alt ='"+issue.assignee+"'class='avatar' src='"+issue.avatarUrl+"'/>");
		  } else {
				return ("<span class='avatar unknown'>?</span>");
		  }

		}

		$('h2', el).html(data.sprintName +" In progress");
		$('.issues', el).empty();
		

		if (data.inprogressIssues.length > 0) {

			data.inprogressIssues.forEach(function(issue) {
			  var listItem = $("<li/>")
			  
			  listItem.append(getAvatar(issue));

			  var $issueData = $("<div class=\"issue-data\"/>");
			  $issueData.append($("<strong/>").addClass("issue-key").append("<span>" + issue.key + "</span>"));
			  $issueData.append($("<strong/>").addClass("issue-type").append($("<img height='20px' width='20px' src='"+issue.typeUrl+"'/>")));
			  $issueData.append($("<strong/>").addClass("issue-priority").append($("<img height='20px' width='20px' src='/images/"+ issue.priorityName.toLowerCase() +".png'/>")));
			  $issueData.append($("<strong/>").addClass("issue-estimate").append(issue.estimate.text));
			  listItem.append($issueData);


			  var $summary = $("<div title='"+ issue.summary +"'/>").addClass("issue-summary").append(issue.summary).appendTo(listItem);

			  $('.issues', el).append(listItem);
			});

		} else {
			$('.issues', el).append(
			  "<div class='no-issues-message'>" +
				"NO ISSUES FOUND" +
				"<div class='smiley-face'>" +
				"☺" +
				"</div>" +
			  "</div>");
		 }

		$('.content', el).show();

    }
};