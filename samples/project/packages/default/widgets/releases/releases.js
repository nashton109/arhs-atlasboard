widget = {

  onData: function(el, data) {
    
	if(data.nextReleaseName){
		$('.releaseInfoContainer', el).show();
		$('.days', el).text(data.nbDaysLeft);
		$('.title', el).text(data.nextReleaseName);
		$('.days', el).css('color','green');
		
		if(data.nbDaysLeft <= data.warningThreshold){
			$('.days', el).css('color','orange');
			
		}
		
		if(data.nbDaysLeft <= data.criticalThreshold){
			$('.days', el).css('color','red');
		}

	} else{
		$('.noRelease', el).show();
		$('.noRelease', el).text("No releases planned");
		
	}
	
	}
};

