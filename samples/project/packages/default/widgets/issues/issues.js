widget = {
    //runs when we receive data from the job



    onData: function(el, data) {

        function getAvatar (issue){
          if(issue.fields.assignee) {
                return $("<img alt = '" + issue.fields.assignee.displayName +
                  "' title='" + issue.fields.assignee.displayName +
                  "' class='avatar' src='"+ issue.fields.assignee.avatarUrls['48x48'] +"'/>");
          } else {
                return ("<span class='avatar unknown'>?</span>");
          }

        }

        $('.issues', el).empty();


        if (data.issues.length){
          $('h2', el).html("Latest Issues");
        } else {
          $('h2', el).html("NO ISSUES");
        }

      if (data.issues.length > 0) {

        data.issues.forEach(function(issue) {
          var listItem = $("<li/>")

          listItem.append(getAvatar(issue));

          var $issueData = $("<div class=\"issue-data\"/>");
          $issueData.append($("<strong/>").addClass("issue-key").append("<a target=_blank href='" + issue.url + "'>" + issue.key + "</a>"));
          $issueData.append($("<strong/>").addClass("issue-owner").append(issue.fields.status.name));
          $issueData.append($("<strong/>").addClass("issue-type").append($("<img height='20px' width='20px' src='"+issue.fields.issuetype.iconUrl+"'/>")));
		  $issueData.append($("<strong/>").addClass("issue-type").append($("<img height='20px' width='20px' src='"+issue.fields.priority.iconUrl+"'/>")));
          listItem.append($issueData);


          var $summary = $("<div title='"+ issue.fields.summary +"'/>").addClass("issue-summary").append(issue.fields.summary).appendTo(listItem);

          $('.issues', el).append(listItem);
        });

      } else {
        $('.issues', el).append(
          "<div class='no-issues-message'>" +
            "NO ISSUES FOUND" +
            "<div class='smiley-face'>" +
            "☺" +
            "</div>" +
          "</div>");
      }

      $('.content', el).show();

    }
};