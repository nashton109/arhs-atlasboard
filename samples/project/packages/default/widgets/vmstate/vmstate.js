widget = {

  onData: function(el, data) {
		
		var hostsState = data.hostsState;
		var config = data.job_config;
		
	
		
		// Sort the VM alphabetically
	    hostsState.sort(function(h1, h2){return h1.name.localeCompare(h2.name);});
		
		// Clean the table
		$('.vmStaterow', el).remove();
		
		hostsState.forEach(function(hostState){
		
			var isHostOff =  (hostState.processorLoad=="off" 
							&& hostState.memoryUsage=="off"
							&& hostState.diskRemainingSpace=="off");
			
			var isCritical = isHostOff;
			
			
			// add new row
			$('.vmState tbody', el).append("<tr class='vmStaterow' name='"+hostState.name+"'></tr>");
			var lastRow = $('.vmState tbody', el).children().last();
			
			// add vm state cell (on/off)
			
			
			var image =  isHostOff ? "/images/vm_off.png":"/images/vm_on.png";
			
							
												
			lastRow.append("<td> <img src="+image+" alt='vm_on' width='30' height='30'> </td>");
			
			// add host name cell 
			lastRow.append("<td>"+hostState.name+"</td>");	
			
			// add processor load cell
			var processorLoad = hostState.processorLoad=="off" ? '-' : +(hostState.processorLoad).toFixed(2);
			var procLoadCell = "<td>"+processorLoad+"</td>" ;
			lastRow.append(procLoadCell);
			
			// add RAM usage cell
			var memoryUsage = hostState.memoryUsage=="off" ? '-' : +(hostState.memoryUsage).toFixed(2);
			var memUsageCell = "<td>"+memoryUsage+"</td>" ;
			lastRow.append(memUsageCell);
			
			if( processorLoad == '-' || memoryUsage == "-" || processorLoad >= config.maxCpu || memoryUsage >= config.maxRam ){
				isCritical = true;
			}
						
			// add Hard disks remaining space cell			
			if(hostState.diskRemainingSpace != "off"){
				lastRow.append("<td></td>");
				// create a span with the number of GB left for each hard disk
				for(var i = 0 ; i < hostState.diskRemainingSpace.length ; i++){
					var diskRemainingSpace = +(hostState.diskRemainingSpace[i]).toFixed(2);
					var diskRemainingSpaceSpan = "<span>"+diskRemainingSpace+"</span>";
					lastRow.children().last().append(diskRemainingSpaceSpan);
					if( diskRemainingSpace <= config.minDiskGB ){
						isCritical = true;
					}
				}
			} else{
				lastRow.append("<td>-</td>");
			}
			
			
			
			if(isCritical){
				blinkRow($('tr[name="'+hostState.name+'"]',el));
			}
			
			
		});
		
		
		function blinkRow(element){
			setInterval(function() {blinkRowAnimation(element); }, 2000);
		}
		
		function blinkRowAnimation(element){
			element.animate({backgroundColor: 'red'}, 1000);
			element.animate({backgroundColor: 'black'}, 1000);
		}
		
		
	
	}
};

