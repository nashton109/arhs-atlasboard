widget = {
    //runs when we receive data from the job

    onData: function(el, data) {

      $('.commits', el).empty();


      if (data.values.length > 0) {
        
          data.values.forEach(function(commit) {
	
                  var listItem = $("<li/>")

                  var $commitData = $("<div class=\"commit-data\"/>");
                  $commitData.append($("<strong/>").addClass("commit-author").append(commit.author.displayName));
                  
                  $commitData.append($("<strong/>").addClass("commit-timestamp").append(moment(commit.authorTimestamp).fromNow()));
                  listItem.append($commitData);


                  var $message = $("<div title='"+ commit.message +"'/>").addClass("issue-summary").append(commit.message).appendTo(listItem);

                  $('.commits', el).append(listItem);
          });
      

      } else {
        $('.commits', el).append(
          "<div class='no-commits-message'>" +
            "NO COMMITS FOUND" +
            "<div class='smiley-face'>" +
            "☺" +
            "</div>" +
          "</div>");
      }

      $('.content', el).show();

     
    }
};